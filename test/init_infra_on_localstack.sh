#!/usr/bin/env bash

# check whether localstack service is available under hostname 'localstack'
# if yes use 'localstack' as the endpoint otherwise use localhost

if curl -Is http://localstack:4566
then
  endpoint_base="localstack"
else
  endpoint_base="localhost"
fi

aws sqs create-queue --queue-name queue-1 --endpoint-url http://${endpoint_base}:4566

aws sqs list-queues --endpoint-url http://${endpoint_base}:4566